export interface Level {
  seed: number
  img?: string
  backgroundColor: string
}