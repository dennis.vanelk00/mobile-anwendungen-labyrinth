import { SafeAreaView, StyleSheet, Text, View } from "react-native"
import React from 'react';
import { TextField } from "../components"

const styles = StyleSheet.create({
  header:{
    top: 0,
    width: '100%',
  },
  textRow: {
    margin: "auto",
    marginTop: "32px"
  },
  row: {
    margin: "auto",
  },
  title: {
    backgroundColor: '#721121',
    color: 'white',
    fontSize: 30,
    paddingBottom: 5,
    paddingTop: 5,
    textAlign: 'center',
  },
  loginButton: {
    marginLeft: "0px",
    marginRight: "auto",
    marginTop: "32px",
    padding: "8px",
  },
  registerButton: {
    marginTop: "32px",
    padding: "8px",
    fontSize: "20px"
  }
});
export const RegScreen = ({navigation}) => {
  const goToMenu = () => {
    navigation.navigate("Menu")
  }
  return (
    <SafeAreaView>
      <View style={styles.header}>
        <Text style={styles.title}>
          Register
        </Text>
      </View>
      <View style={styles.textRow}>
        <TextField placeholder={"Username"}>

        </TextField>
      </View>
      <View style={styles.textRow}>
        <TextField placeholder={"E-Mail"}>

        </TextField>
      </View>
      <View style={styles.textRow}>
        <TextField placeholder={"Password"}>

        </TextField>
        <View style={{display: "flex", flexDirection: "row"}}>
          <View style={{flexGrow: 1}}>

          </View>
          <button style={styles.registerButton} onClick={goToMenu}>
            Register
          </button>
        </View>
      </View>
    </SafeAreaView>
  )
}