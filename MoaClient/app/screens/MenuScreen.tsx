import { SafeAreaView, StyleSheet, Text, Image, View, Dimensions } from "react-native"
import React from 'react';
import { Level } from "../models/Level"
const styles = StyleSheet.create({
  header:{
    top: 0,
    width: '100%',
  },
  title: {
    backgroundColor: '#721121',
    color: 'white',
    fontSize: 30,
    paddingBottom: 5,
    paddingTop: 5,
    textAlign: 'center',
  },
});

const levels: Level[] = [
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "red"
  },
  {
    seed: 245,
    backgroundColor: "blue"
  },
  {
    seed: 245,
    backgroundColor: "lightblue"
  },
  {
    seed: 245,
    backgroundColor: "red"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  },
  {
    seed: 245,
    backgroundColor: "grey"
  }
]

export const MenuScreen = ({navigation}) => {
  const goToGameView = (level: number,levelParam: Level) => {
    navigation.navigate("GameView", {level, levelParam})
  }

  const height = (Dimensions.get("window").height - 144) + "px";
  return (
      <SafeAreaView style={{display: "flex"}}>
        <View style={styles.header}>
          <Text style={styles.title}>
            Menu
          </Text>
        </View>
        <View style={{padding: "32px",
          display: "flex",
          height: height,
          flexDirection: "row",
          flexWrap: "wrap",
          overflow: "scroll"
        }}>

          { levels.map((level, index) => (
            <View key={index} style={{padding: "32px",
              margin: "16px",
              borderStyle: "solid",
              borderColor: "#00000033",
              borderWidth: 1,
              flexGrow: 1,
              backgroundColor: level.backgroundColor
            }} onTouchEnd={() => goToGameView(index + 1, level)}>
              <Text style={{fontSize: "25px"}}>
                {index + 1}
              </Text>
            </View>
          ))}
        </View>
        <View style={{flexGrow: 0, display: "flex", flexDirection: "row", padding: "32px", paddingTop:"16px"}}>
          <View onTouchEnd={() => navigation.navigate("HighScore")} style={{ flexGrow: 0 }}>
            <Image style={{  width: "48px", height: "48px" }} source={{uri: "https://cdn-icons-png.flaticon.com/512/839/839860.png"}} >

            </Image>
          </View>
          <View style={{flexGrow: 1}}>

          </View>
          <View onTouchEnd={() => navigation.navigate("Login")} style={{ flexGrow: 0 }}>
            <Image style={{  width: "48px", height: "48px" }} source={{uri: "https://cdn-icons-png.flaticon.com/512/3889/3889524.png"}} >

            </Image>
          </View>
        </View>
        {/*    */}
      </SafeAreaView>
  )
}