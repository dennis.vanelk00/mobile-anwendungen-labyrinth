import { Image, SafeAreaView, StyleSheet, Text, View } from "react-native"
import React from 'react';
import { AutoImage } from "../components"

const styles = StyleSheet.create({
  header:{
    top: 0,
    width: '100%',
    flexGrow: 0
  },
  title: {
    backgroundColor: '#721121',
    color: 'white',
    fontSize: 30,
    paddingBottom: 5,
    paddingTop: 5,
    textAlign: 'center',
  },
});

export const GameViewScreen = ({navigation, route}) => {
  const {level, levelParams} = route.params;
  return (
      <SafeAreaView style={{display: "flex", height: "99%"}}>
        <View style={styles.header}>
          <Text style={styles.title}>
            Level {level}
          </Text>
        </View>
        <View style={{flexGrow: 0, maxHeight: "400px", maxWidth: "400px", width: "100%", height: "400px", padding: "32px" }}>
          <View style={{
            backgroundColor: "#00000033",
            width: "100%",
            height: "100%",
            alignContent: "center"
          }}>
            <Text style={{fontSize: "30px", margin: "auto"}}>
              Game
            </Text>
          </View>
        </View>
        <View style={{flexGrow: 1}}>

        </View>
        <View style={{flexGrow: 0, paddingHorizontal:"32px", display: "flex", flexDirection: "row"}}>
          <View style={{flexGrow: 0}} onTouchEnd={() => navigation.navigate("Menu")}>
            <Image style={{height: "48px", width: "48px" }} source={{ uri: "https://cdn-icons-png.flaticon.com/512/93/93634.png" }}>

            </Image>
          </View>
          <View style={{flexGrow: 1, height: "48px"}}>
            <Text style={{ margin: "auto", lineHeight: "48px", fontSize: "48px"}}>
              00:34
            </Text>
          </View>
          <View style={{flexGrow: 0}}>
            <Image style={{height: "48px", width: "48px" }} source={{ uri: "https://cdn-icons-png.flaticon.com/512/5505/5505845.png" }}>

            </Image>
          </View>
        </View>

      </SafeAreaView>
  )
}