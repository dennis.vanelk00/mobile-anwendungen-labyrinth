import React from 'react';
import {StyleSheet, Button, View, SafeAreaView, Text, FlatList} from 'react-native';



export const HighScoreScreen = ({navigation}) => {

    const returnToMenu = () => {
        navigation.navigate("Menu")
    }

    return(
        <SafeAreaView>
            <View style={styles.header}>
                <Text style={styles.title}>
                    Highscore
                </Text>
            </View>
            <View>
                <View style={styles.backbutton}>
                    <Button title={"X"} onPress={returnToMenu}/>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.list}>
                    <View style={styles.column}>
                        <Text style={styles.name}>Level</Text>
                        <FlatList data={[
                            {key: 1},
                            {key: 2},
                            {key: 3},
                            {key: 4},
                        ]} renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
                        />
                    </View>
                    <View style={styles.column}>
                        <Text style={styles.name}>Time</Text>
                        <FlatList data={[
                            {key: "00:10"},
                            {key: "00:10"},
                            {key: "00:10"},
                            {key: "00:10"},
                        ]} renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
                        />
                    </View>
                    <View style={styles.column}>
                        <Text style={styles.name}>Fails</Text>
                        <FlatList data={[
                            {key: 2},
                            {key: 2},
                            {key: 2},
                            {key: 2},
                        ]} renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
                        />
                    </View>
                    <View style={styles.column}>
                        <Text style={styles.name}>Highscore</Text>
                        <FlatList data={[
                            {key: 250},
                            {key: 250},
                            {key: 250},
                            {key: 250},
                        ]} renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
                        />
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    backbutton: {
        fontSize: 60,
        left: 0,
        margin: 10,
        maxHeight: 50,
        maxWidth: 50,
        position: "relative",
    },
    column: {
        backgroundColor: "#FFC07F",
        display: "table",
        textAlign: "center",
        width: "25%",
    },
    header:{
        top: 0,
        width: '100%',
    },
    item: {
        backgroundColor: "#FFCF99",
        borderBottomColor: "black",
        borderBottomWidth: 1,
        fontSize: 25,
    },
    list: {
        flexDirection: "row",
        flexWrap: "wrap",
    },
    name: {
        fontSize: 30,
        fontWeight: "bold",
        width: "100%",
    },
    row: {
        margin: "auto",
        minWidth: 760,
    },
    title: {
        backgroundColor: '#721121',
        color: 'white',
        fontSize: 30,
        paddingBottom: 5,
        paddingTop: 5,
        textAlign: 'center',
    },
});
