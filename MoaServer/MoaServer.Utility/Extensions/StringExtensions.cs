﻿namespace MoaServer.Utility.Extensions;

public static class StringExtensions
{
    private const string PossibleCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static readonly Random Random = new Random();
    
    public static string CreateRandom(this string str, int length = 16)
    {
        for (var i = 0; i < length; i++)
        {
            str += PossibleCharacters[Random.Next(PossibleCharacters.Length)];
        }

        return str;
    }
}