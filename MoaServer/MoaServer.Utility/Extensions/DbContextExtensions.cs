﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage;

namespace MoaServer.Utility.Extensions;

public static class DbContextExtensions
{
    public static async Task<T> TransactionAsync<T>(this DbContext context, Func<IDbContextTransaction, ValueTask<EntityEntry<T>>> action)
        where T : class
    {
        var transaction = await context.Database.BeginTransactionAsync();
        try
        {
            var result = await action(transaction);
            await transaction.CommitAsync();
            await context.SaveChangesAsync();
            return result.Entity;
        }
        catch
        {
            await transaction.RollbackAsync();
            throw;
        }
    }
    
    public static async Task TransactionAsync(this DbContext context, Action<IDbContextTransaction> action)
    {
        var transaction = await context.Database.BeginTransactionAsync();
        try
        {
            action(transaction);
            await transaction.CommitAsync();
            await context.SaveChangesAsync();
        }
        catch
        {
            await transaction.RollbackAsync();
            throw;
        }
    }
}