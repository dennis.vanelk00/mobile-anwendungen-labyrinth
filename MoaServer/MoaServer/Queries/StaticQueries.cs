﻿using GraphQL;
using Microsoft.EntityFrameworkCore;
using MoaServer.GraphQL.Attributes;

namespace MoaServer.Queries;

public static class StaticQueries
{
    [Query(Name = "find{SingleTypeName}ById")]
    public static ValueTask<T?> FindById<T>(
        IResolveFieldContext fieldContext, 
        DbContext dbContext,
        [Input] Guid id
    )
        where T : class
    {
        return dbContext.FindAsync<T>(id);
    }
    [Query(Name = "get{PluralTypeName}")]
    public static Task<List<T>> GetList<T>(
        IResolveFieldContext fieldContext, 
        DbContext dbContext,
        [Input] int? skip,
        [Input] int? take
    )
        where T : class
    {
        return dbContext.Set<T>()
            .Skip(skip ?? 0)
            .Take(take ?? 30)
            .ToListAsync();
    }
}