﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MoaServer.Model.V1.Authorization;
using MoaServer.Model.V1.Options;

namespace MoaServer.Middlewares;

public class JwtAuthenticationMiddleware : IMiddleware
{
    private static readonly List<Guid> _invalidatedCache = new();

    private readonly JwtOptions _jwtOptions;
    private readonly DbContext _dbContext;
    public JwtAuthenticationMiddleware(JwtOptions options, DbContext context)
    {
        _jwtOptions = options;
        _dbContext = context;
    }
    
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        var authToken = context.Request.Headers["Authorization"]
            .FirstOrDefault(x => x.StartsWith("Bearer"))?[7..];

        if (authToken == null)
        {
            await next(context);
            return;
        }
        
        var tokenHandler = new JwtSecurityTokenHandler();
        var validationParameters = new TokenValidationParameters()
        {
            ValidateLifetime = true, 
            ValidateAudience = true, 
            ValidateIssuer = true,
            ValidIssuer = _jwtOptions.Issuer,
            ValidAudience = _jwtOptions.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Key))
        };

        JwtSecurityToken jwtToken;
        try
        {
            _ = tokenHandler.ValidateToken(authToken, validationParameters, out var unknownToken);
            jwtToken = (JwtSecurityToken)unknownToken;
        }
        catch
        {
            context.Response.StatusCode = 401;
            await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes("Bearer token is invalid"));
            return;
        }

        var jti = Guid.Parse(jwtToken.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value);

        if (!_invalidatedCache.Contains(jti))
        {
            var iJwt = await _dbContext.Set<InvalidatedJwt>().FindAsync(jti);

            if (iJwt != null)
            {
                _invalidatedCache.Insert(0, iJwt.Id);
                if (_invalidatedCache.Count > 500)
                {
                    _invalidatedCache.RemoveRange(500, _invalidatedCache.Count - 500);
                }
                
                context.Response.StatusCode = 401;
                await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes("Bearer token is invalid"));
                return;
            }
        }
        else
        {
            context.Response.StatusCode = 401;
            await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes("Bearer token is invalid"));
            return;
        }

        await next(context);
    }
}