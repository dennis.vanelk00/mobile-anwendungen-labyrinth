using System.Text;
using Microsoft.EntityFrameworkCore;
using MoaServer.GraphQL.Extensions;
using MoaServer.Middlewares;
using MoaServer.Model.V1.Common;
using MoaServer.Model.V1.EFCore;
using MoaServer.Model.V1.Options;
using MoaServer.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddElasticQL(options =>
{
    options.ApiEndpoint = "/graphql";
    options.UiEndpoint = "/ui";
    options.UiEnabled = true;
});

builder.Services.AddSingleton<InvalidJwtCleanUpService>();

builder.Services.AddTransient<JwtAuthenticationMiddleware>();
builder.Services.AddSingleton(sp => sp.GetRequiredService<IConfiguration>().GetValue<ConnectionStringsOptions>("ConnectionStrings"));
builder.Services.AddSingleton<JwtOptions>(sp => 
    sp.GetRequiredService<IConfiguration>()
        .GetSection("JwtOptions")
        .Get<JwtOptions>()
);

builder.Services.AddDbContext<MoaContext>(x => 
    x.UseNpgsql(builder.Configuration.GetConnectionString("MoaDb"))
        .UseSnakeCaseNamingConvention()
);
builder.Services.AddDbContext<DbContext, MoaContext>(x => 
    x.UseNpgsql(builder.Configuration.GetConnectionString("MoaDb"))
        .UseSnakeCaseNamingConvention()
);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<JwtAuthenticationMiddleware>();
app.UseAuthorization();

app.MapControllers();

app.UseElasticQL();

var scope = app.Services.CreateScope();
var dbContext = scope
    .ServiceProvider.GetRequiredService<DbContext>();

dbContext.Database.Migrate();

scope.ServiceProvider.GetRequiredService<InvalidJwtCleanUpService>();

app.Run();