﻿using Microsoft.EntityFrameworkCore;
using MoaServer.Model.V1.Authorization;
using MoaServer.Model.V1.Options;
using MoaServer.Utility.Extensions;

namespace MoaServer.Services;

public class InvalidJwtCleanUpService
{
    private readonly IServiceProvider _provider;
    private readonly JwtOptions _jwtOptions;
    
    public InvalidJwtCleanUpService(IServiceProvider provider, JwtOptions jwtOptions)
    {
        _provider = provider;
        _jwtOptions = jwtOptions;
        _ = CleanUp();
    }

    private async Task CleanUp()
    {
        do
        {
            try
            {
                var dbContext = _provider.CreateScope().ServiceProvider.GetRequiredService<DbContext>();

                var invalidated = await dbContext.Set<InvalidatedJwt>()
                    .Where(x => x.ExpirationTime < DateTime.UtcNow)
                    .ToListAsync();

                if (invalidated.Count > 0)
                {
                    await dbContext.TransactionAsync(_ => dbContext.RemoveRange(invalidated));
                }

            }
            catch(Exception ex)
            {
                throw;
            }
            await Task.Delay(_jwtOptions.ExpirationTime * 1000);
        } while (true);
    }
}