﻿using System.Numerics;
using System.Reflection;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using MoaServer.GraphQL.Attributes;

namespace MoaServer.GraphQL.Utility;

public class StaticData
{
    public static readonly IReadOnlyDictionary<Type, Type> TypeToGraphTypeMap = new Dictionary<Type, Type>()
    {
        {typeof(string), typeof(StringGraphType)},
        {typeof(int), typeof(IntGraphType)},
        {typeof(float), typeof(FloatGraphType)},
        {typeof(bool), typeof(BooleanGraphType)},
        {typeof(BigInteger), typeof(BigIntGraphType)},
        {typeof(byte), typeof(ByteGraphType)},
        {typeof(DateTime), typeof(DateTimeGraphType)},
        {typeof(DateOnly), typeof(DateOnlyGraphType)},
        {typeof(DateTimeOffset), typeof(DateTimeOffsetGraphType)},
        {typeof(decimal), typeof(DecimalGraphType)},
        {typeof(Guid), typeof(GuidGraphType)},
        {typeof(Half), typeof(HalfGraphType)},
        {typeof(long), typeof(LongGraphType)},
        {typeof(TimeSpan), typeof(TimeSpanMillisecondsGraphType)},
        {typeof(sbyte), typeof(SByteGraphType)},
        {typeof(short), typeof(ShortGraphType)},
        {typeof(TimeOnly), typeof(TimeOnlyGraphType)},
        {typeof(uint), typeof(UIntGraphType)},
        {typeof(ulong), typeof(ULongGraphType)},
        {typeof(Uri), typeof(UriGraphType)},
        {typeof(ushort), typeof(UShortGraphType)}
    };
    
    
    public static Func<IResolveFieldContext<object>, Task<object?>> MethodBuilder(MethodBase method)
    {
        return async (contextObj) =>
        {
            var context = (IResolveFieldContext)contextObj;
            var parameterInfos = method.GetParameters();
            var parameters = new object?[parameterInfos.Length];
            for (var i = 0; i < parameterInfos.Length; i++)
            {
                var parameterInfo = parameterInfos[i];
                if (parameterInfo.ParameterType.IsAssignableTo(typeof(IResolveFieldContext)))
                {
                    parameters[i] = context;
                    continue;
                }
                if (parameterInfo.ParameterType.IsAssignableTo(typeof(HttpContext)))
                {
                    parameters[i] = context.RequestServices!
                        .GetRequiredService<IHttpContextAccessor>()
                        .HttpContext;
                    continue;
                }

                var inputOptions = parameterInfo.GetCustomAttribute<InputAttribute>();
                if (inputOptions == null)
                {
                    var service = context.RequestServices!.GetService(parameterInfo.ParameterType);
                    if (service != null)
                    {
                        parameters[i] = service;
                        continue;
                    }
                }
                parameters[i] = context.GetArgument(
                    parameterInfo.ParameterType, 
                    inputOptions?.Name ?? parameterInfo.Name!);
            }

            for (var i = 0; i < parameters.Length; i++)
            {
                if (Nullable.GetUnderlyingType(parameterInfos[i].ParameterType) == null
                    && parameters[i] == null)
                {
                    throw new ExecutionError("argument: " + parameterInfos[i].Name + ", is required");
                }
            }
            
            return await (method.Invoke(null, parameters) as dynamic)!;
        };
    }
}