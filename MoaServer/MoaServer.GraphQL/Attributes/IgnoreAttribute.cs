﻿namespace MoaServer.GraphQL.Attributes;

[AttributeUsage(AttributeTargets.Property)]
public class IgnoreAttribute : Attribute
{
    
}