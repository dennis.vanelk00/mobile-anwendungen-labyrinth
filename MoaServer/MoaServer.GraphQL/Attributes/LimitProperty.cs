﻿using MoaServer.GraphQL.Model.Enums;

namespace MoaServer.GraphQL.Attributes;

[AttributeUsage(AttributeTargets.Property)]
public class LimitPropertyAttribute : Attribute
{
    public CRUD Crud { get; set; } = CRUD.None;
}