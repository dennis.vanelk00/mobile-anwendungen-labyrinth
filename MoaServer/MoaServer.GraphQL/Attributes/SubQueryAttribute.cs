﻿namespace MoaServer.GraphQL.Attributes;

[AttributeUsage(AttributeTargets.Method)]
public class SubQueryAttribute : Attribute
{
    
}