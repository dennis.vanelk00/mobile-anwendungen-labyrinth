﻿using MoaServer.GraphQL.Model.Enums;

namespace MoaServer.GraphQL.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class GraphTypeAttribute : Attribute
{
    public CRUD CrudType { get; set; } = CRUD.None;
    public bool ExposeAsList { get; set; } = false;
}