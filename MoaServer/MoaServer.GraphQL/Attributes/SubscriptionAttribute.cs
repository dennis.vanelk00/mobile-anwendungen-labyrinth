﻿namespace MoaServer.GraphQL.Attributes;

[AttributeUsage(AttributeTargets.Method)]
public class SubscriptionAttribute : Attribute
{
    
}