﻿namespace MoaServer.GraphQL.Attributes;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Parameter, AllowMultiple = true)]
public class InputAttribute : Attribute
{
    public string? Name { get; set; }
    public string? Description { get; set; }
    public string? Obsolete { get; set; }
}