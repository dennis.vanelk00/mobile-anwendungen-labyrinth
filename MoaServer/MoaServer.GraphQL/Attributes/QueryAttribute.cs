﻿namespace MoaServer.GraphQL.Attributes;

[AttributeUsage(AttributeTargets.Method)]
public class QueryAttribute : Attribute
{
    public string? Name { get; set; }
    public string? Description { get; set; }
    public string? Obsolete { get; set; }
}