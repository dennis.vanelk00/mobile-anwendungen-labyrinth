﻿using System.Reflection;
using GraphQL;
using GraphQL.Types;
using MoaServer.GraphQL.Attributes;
using MoaServer.GraphQL.Extensions;
using MoaServer.GraphQL.Utility;

namespace MoaServer.GraphQL.Services;

public class ElasticQLMutationService : ObjectGraphType
{
    public ElasticQLMutationService()
    {
        var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(x => x.GetTypes())
            .Where(x => x.GetCustomAttribute<GraphTypeAttribute>() != null)
            .Select(x => new KeyValuePair<Type,GraphTypeAttribute>(x, x.GetCustomAttribute<GraphTypeAttribute>()!));

        foreach (var (type, graphTypeOptions) in types)
        {
            var queries = type.GetMethods(BindingFlags.Static | BindingFlags.Public)
                .Where(x => x.GetCustomAttribute<MutationAttribute>() != null)
                .Select(x => new KeyValuePair<MethodInfo, MutationAttribute>(x, x.GetCustomAttribute<MutationAttribute>()!));

            foreach (var (method, queryOptions) in queries)
            {
                if(!method.ReturnType.IsAssignableTo(typeof(Task))) 
                    throw new ExecutionError("can't execute queries which are not async");

                var genericArguments = method.ReturnType.GetGenericArguments();
                if (genericArguments.Length == 0)
                    throw new ExecutionError("queries have to return a value");

                var isList = method.ReturnType.TryGetBaseType(out var returnType);

                var fb = this.CreateFieldBuilder(returnType, queryOptions.Name ?? method.Name);
                if(queryOptions.Description != null) fb.Description(queryOptions.Description);
                
                if(queryOptions.Obsolete != null) fb.DeprecationReason(queryOptions.Obsolete);

                foreach (var (parameterInfo, inputAttribute) in method.GetParameters()
                             .Where(x => x.GetCustomAttribute<InputAttribute>() != null)
                             .Select(x => new KeyValuePair<ParameterInfo, InputAttribute>(x, x.GetCustomAttribute<InputAttribute>()!))
                         )
                {
                    fb.Argument(parameterInfo.ParameterType.GetGraphType(), inputAttribute.Name ?? parameterInfo.Name!);
                }
                
                fb.ResolveAsync(StaticData.MethodBuilder(method));
            }
        }
    }
}