﻿using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace MoaServer.GraphQL.Services;

public class ElasticQLSchemaService : Schema
{
    public ElasticQLSchemaService(IServiceProvider provider) : base(provider)
    {
        Query = provider.GetRequiredService<ElasticQLQueryService>();
        Mutation = provider.GetRequiredService<ElasticQLMutationService>();
    }
}