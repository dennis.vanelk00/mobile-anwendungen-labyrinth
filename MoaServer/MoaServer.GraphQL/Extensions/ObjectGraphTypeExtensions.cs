﻿using System.Reflection;
using GraphQL.Builders;
using GraphQL.Types;
using MoaServer.GraphQL.Model.Common;
using MoaServer.Utility.Extensions;

namespace MoaServer.GraphQL.Extensions;

public static class ObjectGraphTypeExtensions
{

    public static FieldBuilder<object, object> CreateFieldBuilder(this ObjectGraphType obj, Type returnType, string fieldName)
        => CreateFieldBuilderExecute(
            obj, 
            returnType, 
            fieldName, 
            typeof(ObjectGraphType)
                .GetMethods()
                .First(x => x.Name == "Field"
                            && x.GetGenericArguments().Length == 2
                            && x.GetParameters().Length == 1)
                .GetGenericMethodDefinition()
            );

    public static FieldBuilder<object, object> CreateFieldBuilder<T>(this ObjectGraphType<T> obj, Type returnType,
        string fieldName)
        => CreateFieldBuilderExecute(
            obj, 
            returnType, 
            fieldName,
            typeof(ObjectGraphType<T>)
                .GetMethods()
                .First(x => x.Name == "Field"
                            && x.GetGenericArguments().Length == 2
                            && x.GetParameters().Length == 1)
                .GetGenericMethodDefinition()
            );
    
    public static FieldBuilder<object, object> CreateFieldBuilder(this IObjectGraphType obj, Type returnType, string fieldName)
        => CreateFieldBuilderExecute(
            obj, 
            returnType, 
            fieldName, 
            typeof(ObjectGraphType)
                .GetMethods()
                .First(x => x.Name == "Field"
                            && x.GetGenericArguments().Length == 2
                            && x.GetParameters().Length == 1)
                .GetGenericMethodDefinition()
            );
    
    private static FieldBuilder<object, object> CreateFieldBuilderExecute(
        this IObjectGraphType obj, 
        Type returnType,
        string fieldName,
        MethodInfo fieldMethodDefinition
        )
    {
        var returnGraphType = returnType.GetGraphType();
        var b = fieldMethodDefinition
            .MakeGenericMethod(returnGraphType, typeof(object))
            .Invoke(obj, new object[] { fieldName });

        return (b as FieldBuilder<object, object>)!;
    }
}