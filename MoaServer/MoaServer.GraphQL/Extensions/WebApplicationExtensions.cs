﻿using GraphQL;
using GraphQL.Instrumentation;
using GraphQL.Server.Ui.Altair;
using GraphQL.Transport;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MoaServer.GraphQL.Model.Common;

namespace MoaServer.GraphQL.Extensions;

public static class WebApplicationExtensions
{
    public static void UseElasticQL(this WebApplication app)
    {
        app.UseWebSockets();

        var scope = app.Services.CreateScope();
        var options = scope.ServiceProvider.GetRequiredService<MoaGraphQLOptions>();

        app.UseGraphQL(options.ApiEndpoint);

        if (options.UiEnabled)
        {
            app.UseGraphQLAltair(options.UiEndpoint, new AltairOptions()
            {
                GraphQLEndPoint = options.ApiEndpoint
            });
        }
        
        // app.MapPost(options.ApiEndpoint, async (
        //     [Microsoft.AspNetCore.Mvc.FromServices] IDocumentExecuter executer,
        //     [Microsoft.AspNetCore.Mvc.FromServices] ISchema schema,
        //     [Microsoft.AspNetCore.Mvc.FromServices] IHttpContextAccessor contextAccessor,
        //     [Microsoft.AspNetCore.Mvc.FromServices] ILogger logger,
        //     [FromBody] GraphQLRequest request
        // ) =>
        // {
        //     var startTime = DateTime.UtcNow;
        //     var result = await executer.ExecuteAsync(opts =>
        //     {
        //         opts.Schema = schema;
        //         opts.Query = request.Query;
        //         opts.Variables = request.Variables; 
        //         opts.RequestServices = contextAccessor.HttpContext.RequestServices;
        //         opts.CancellationToken = contextAccessor.HttpContext.RequestAborted;
        //     });
        //
        //     if (options.MetricsEnabled)
        //     {
        //         result.EnrichWithApolloTracing(startTime);
        //     }
        //     return result;
        // });
    }
}