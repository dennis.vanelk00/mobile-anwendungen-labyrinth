﻿using System.Collections;
using GraphQL.Types;
using MoaServer.GraphQL.Model.Common;
using MoaServer.GraphQL.Utility;
using MoaServer.Utility.Extensions;

namespace MoaServer.GraphQL.Extensions;

public static class TypeExtensions
{
    public static bool TryGetBaseType(this Type type, out Type? baseType)
    {
        if (type.IsAssignableTo(typeof(IElasticQLType)))
        {
            type = type.GetGenericArguments().First();
        }

        if (type.IsAssignableTo(typeof(Task)))
        {
            type = type.GetGenericArguments().First();
        }

        baseType = type.GetElementType();
        
        if (type == typeof(string)
            || !type.IsAssignableTo(typeof(IEnumerable))
            || (type.GetGenericArguments().Length == 0)
                && baseType == null)
        {
            baseType = type;
            return false;
        }
        
        baseType ??= type.GetGenericArguments().First();
        return true;
    }
    
    public static Type GetGraphType(this Type type)
    {
        var isList = type.TryGetBaseType(out var baseType);

        if (!StaticData.TypeToGraphTypeMap.TryGetValue(baseType, out var graphType))
        {
            graphType = typeof(ElasticQLGraphType<>)
                .GetGenericTypeDefinition()
                .MakeGenericType(baseType);
        }

        if (isList)
        {
            graphType = typeof(ListGraphType<>)
                .GetGenericTypeDefinition()
                .MakeGenericType(graphType);
        }

        return graphType;
    }
}