﻿
using GraphQL;
using Microsoft.Extensions.DependencyInjection;
using MoaServer.GraphQL.Model;
using MoaServer.GraphQL.Model.Common;
using MoaServer.GraphQL.Services;

namespace MoaServer.GraphQL.Extensions;

public static class ServiceCollectionExtensions
{
    public static void AddElasticQL(this IServiceCollection services, Action<MoaGraphQLOptions>? configure = null)
    {
        var options = new MoaGraphQLOptions();
        configure?.Invoke(options);

        services.AddHttpContextAccessor();
        
        services.AddGraphQL(b =>
            b.AddSystemTextJson()
                .UseMemoryCache(x =>
                {
                    x.Clock = options.CacheOptions.Clock;
                    x.ExpirationScanFrequency = options.CacheOptions.ExpirationScanFrequency;
                    x.SlidingExpiration = options.CacheOptions.SlidingExpiration;
                    x.CompactionPercentage = options.CacheOptions.CompactionPercentage;
                    x.SizeLimit = options.CacheOptions.SizeLimit;
                })
#if DEBUG
                .AddErrorInfoProvider(o => o.ExposeData = true)
#else
                .AddErrorInfoProvider(o => o.ExposeData = false)
#endif
                .AddSchema<ElasticQLSchemaService>()
            );
        
        services.AddSingleton(sp => options);
        services.AddTransient(typeof(ElasticQLGraphType<>));
        services.AddTransient<ElasticQLQueryService>();
        services.AddTransient<ElasticQLMutationService>();
    }
}