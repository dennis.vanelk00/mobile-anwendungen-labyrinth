﻿namespace MoaServer.GraphQL.Model.Enums;

[Flags]
public enum CRUD
{
    None = 0,
    Create = 1,
    Read = 2,
    Update = 4,
    Delete = 8,
    Crud = 15
}