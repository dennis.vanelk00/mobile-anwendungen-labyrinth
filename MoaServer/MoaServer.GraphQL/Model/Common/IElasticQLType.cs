﻿namespace MoaServer.GraphQL.Model.Common;

public interface IElasticQLType
{
    Type Type { get; }
}