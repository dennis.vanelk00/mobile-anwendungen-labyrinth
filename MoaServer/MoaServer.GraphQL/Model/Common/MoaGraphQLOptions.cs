﻿using GraphQL.Caching;

namespace MoaServer.GraphQL.Model.Common;

public class MoaGraphQLOptions
{
    public string ApiEndpoint { get; set; } = "/graphql";
    public string UiEndpoint { get; set; } = "/ui/graphql";
    public bool UiEnabled { get; set; } = false;
    public bool MetricsEnabled { get; set; } = false;
    
    public MemoryDocumentCacheOptions CacheOptions { get; set; } = new ()
    {
        SizeLimit = 10 * 1024 * 1024,
        SlidingExpiration = null
    };
}