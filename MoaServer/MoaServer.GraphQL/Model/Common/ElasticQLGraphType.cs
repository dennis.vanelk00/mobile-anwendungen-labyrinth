﻿using System.Numerics;
using System.Reflection;
using System.Reflection.Emit;
using System.Reflection.Metadata;
using GraphQL.Builders;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using MoaServer.GraphQL.Extensions;
using MoaServer.Utility.Extensions;

namespace MoaServer.GraphQL.Model.Common;

public class ElasticQLGraphType<T> : ObjectGraphType<T>, IElasticQLType
    where T : class
{
    public Type Type { get; private set; } = typeof(T);

    public ElasticQLGraphType()
    {
        Name = Type.Name;
        
        var props = Type.GetProperties();
        foreach (var prop in props)
        {
            var fb = this.CreateFieldBuilder(prop.PropertyType, prop.Name);
        }
    }
}