﻿using GraphQL.Types;

namespace MoaServer.GraphQL.Model.Common;

public class ElasticQLInputGraphType<T> : InputObjectGraphType<T>, IElasticQLType
    where T : class
{
    public Type Type { get; private set; } = typeof(T);
    
    public ElasticQLInputGraphType()
    {
        
    }
}