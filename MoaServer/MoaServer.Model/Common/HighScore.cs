﻿using System.ComponentModel.DataAnnotations.Schema;
using MoaServer.GraphQL.Attributes;
using MoaServer.GraphQL.Model.Enums;

namespace MoaServer.Model.V1.Common;

[GraphType(CrudType = CRUD.Read | CRUD.Create)]
public class HighScore
{
    public Guid Id { get; set; }
    public long Score { get; set; }

    private DateTime? _ts;
    
    [LimitProperty(Crud = CRUD.Read)]
    public DateTime TimeStamp
    {
        get => _ts ?? DateTime.UtcNow;
        set
        {
            if (_ts != null) 
                throw new Exception("the timestamp of a high-score can't be updated");
            _ts = value;
        }
    }

    [ForeignKey(nameof(User))]
    public Guid UserId { get; set; }
    public User? User { get; set; }
}