﻿using System.Security.Cryptography;
using System.Text;
using GraphQL;
using Microsoft.EntityFrameworkCore;
using MoaServer.GraphQL.Attributes;
using MoaServer.GraphQL.Model.Enums;
using MoaServer.Model.V1.Options;
using MoaServer.Utility.Extensions;

namespace MoaServer.Model.V1.Common;

[GraphType(CrudType = CRUD.Read)]
public class User
{
    private static readonly SHA256 Sha256 = SHA256.Create();
    
    public Guid Id { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    
    [GraphQL.Attributes.Ignore]
    public byte[] PasswordHash { get; set; }
    [GraphQL.Attributes.Ignore]
    public string Salt { get; set; }

    public List<HighScore>? HighScores { get; set; }

    [Mutation(Name = "registerUser")]
    public static async Task<User> RegisterUser(
        DbContext context,
        [Input] string username,
        [Input] string email,
        [Input] string password
        )
    {
        if (await context.Set<User>().AnyAsync(x => x.Username == username))
        {
            throw new ExecutionError((int)ErrorCode.UserAlreadyExists + ": the user already exists");
        }

        var salt = "".CreateRandom(40);
        
        var user = new User()
        {
            Username = username,
            Email = email,
            PasswordHash = Sha256.ComputeHash(Encoding.UTF8.GetBytes(password + salt)),
            Salt = salt
        };

        user = await context.TransactionAsync(_ => context.AddAsync(user));

        return user;
    }
}