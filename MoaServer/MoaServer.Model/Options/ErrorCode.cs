﻿namespace MoaServer.Model.V1.Options;

public enum ErrorCode
{
    Unknown = 0,
    UserNotFound = 1,
    PasswordWrong = 2,
    RefreshTokenNotValid = 3,
    Unauthorized = 4,
    UserAlreadyExists = 5
}