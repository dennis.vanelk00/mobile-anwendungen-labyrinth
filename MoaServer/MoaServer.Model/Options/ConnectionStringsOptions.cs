﻿namespace MoaServer.Model.V1.Options;

public class ConnectionStringsOptions
{
    public string MoaDb { get; set; }
}