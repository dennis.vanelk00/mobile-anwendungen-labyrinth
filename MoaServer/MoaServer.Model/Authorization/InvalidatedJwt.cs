﻿namespace MoaServer.Model.V1.Authorization;

public class InvalidatedJwt
{
    public Guid Id { get; set; }
    public DateTime ExpirationTime { get; set; }
}