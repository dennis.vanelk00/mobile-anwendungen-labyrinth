﻿using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using GraphQL;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MoaServer.GraphQL.Attributes;
using MoaServer.Model.V1.Common;
using MoaServer.Model.V1.Options;
using MoaServer.Utility.Extensions;

namespace MoaServer.Model.V1.Authorization;

[GraphType]
public class Authorization
{
    private static readonly SHA256 Sha256 = SHA256.Create();

    public string AuthorizationToken { get; set; }
    public string RefreshToken { get; set; }
    public int ExpirationTime { get; set; }

    [Query]
    public static async Task<Authorization> Authorize(
        DbContext dbContext, 
        JwtOptions jwtOptions,
        [Input] string username, 
        [Input] string password
        )
    {
        try
        {
            var users = await dbContext.Set<User>().ToListAsync();
            
            var user = await dbContext.Set<User>()
                .FirstOrDefaultAsync(x => x.Username == username);

            if (user == null)
                throw new ExecutionError((int)ErrorCode.UserNotFound + ": user does not exist");

            password += user.Salt;

            var hash = Sha256.ComputeHash(Encoding.UTF8.GetBytes(password));

            if (user.PasswordHash.Length != hash.Length)
                throw new ExecutionError((int)ErrorCode.PasswordWrong + ": the given password was wrong");

            if (user.PasswordHash.Where((t, i) => t != hash[i]).Any())
            {
                throw new ExecutionError((int)ErrorCode.PasswordWrong + ": the given password was wrong");
            }

            return CreateResponse(jwtOptions, user);
        }
        catch(Exception ex)
        {
            throw;
        }
    }

    [Query]
    public static async Task<Authorization> RefreshSession(
        IResolveFieldContext fieldContext,
        DbContext dbContext, 
        JwtOptions jwtOptions,
        [Input] string refreshToken
        )
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var validationParameters = new TokenValidationParameters()
        {
            ValidateLifetime = false, 
            ValidateAudience = true, 
            ValidateIssuer = true,
            ValidIssuer = jwtOptions.Issuer,
            ValidAudience = jwtOptions.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Key))
        };

        try
        {
            _ = tokenHandler.ValidateToken(refreshToken, validationParameters, out var unknownToken);
            var token = (unknownToken as JwtSecurityToken)!;
            await StaticInvalidateToken(
                dbContext, 
                Guid.Parse(token.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value),
                DateTime.Parse(token.Claims.First(x => x.Type == JwtRegisteredClaimNames.Exp).Value)
                );
            
            var id = Guid.Parse(token.Claims.First(x => x.Type == JwtRegisteredClaimNames.Sub).Value);
            var user = await dbContext.FindAsync<User>(id);
            return CreateResponse(jwtOptions, user!);
        }
        catch
        {
            throw new ExecutionError(ErrorCode.RefreshTokenNotValid + ": refresh-token is not valid");
        }
    }

    [Mutation]
    public static async Task<string> InvalidateToken(
        HttpContext httpContext,
        DbContext dbContext, 
        JwtOptions jwtOptions
        )
    {
        var authToken = httpContext.Request.Headers["Authorization"]
            .FirstOrDefault(x => x.StartsWith("Bearer"))![7..];
        
        var tokenHandler = new JwtSecurityTokenHandler();
        var validationParameters = new TokenValidationParameters()
        {
            ValidateLifetime = true, 
            ValidateAudience = true, 
            ValidateIssuer = true,
            ValidIssuer = jwtOptions.Issuer,
            ValidAudience = jwtOptions.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Key))
        };
        
        _ = tokenHandler.ValidateToken(authToken, validationParameters, out var unknownToken);
        var token = (unknownToken as JwtSecurityToken)!;
        var exp = DateTimeOffset.FromUnixTimeSeconds(
            long.Parse(token.Claims.First(x => x.Type == JwtRegisteredClaimNames.Exp)
                .Value)
            ).UtcDateTime;
        await StaticInvalidateToken(
            dbContext, 
            Guid.Parse(token.Claims.First(x => x.Type == JwtRegisteredClaimNames.Jti).Value),
            exp
            );
        
        return "successfully invalidated all tokens connected to the session";
    } 
    
    private static Task StaticInvalidateToken(DbContext context, Guid jti, DateTime expirationTime) 
        =>  context.TransactionAsync(_ => context.AddAsync(
            new InvalidatedJwt
            {
                Id = jti,
                ExpirationTime = expirationTime
            }));

    private static Authorization CreateResponse(JwtOptions jwtOptions, User user)
    {
        var jti = Guid.NewGuid();
        var issuer = jwtOptions.Issuer;
        var audience = jwtOptions.Audience;
        var key = Encoding.ASCII.GetBytes(jwtOptions.Key);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti,
                    jti.ToString())
            }),
            Expires = DateTime.UtcNow.AddSeconds(jwtOptions.ExpirationTime),
            Issuer = issuer,
            Audience = audience,
            SigningCredentials = new SigningCredentials
            (new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha512Signature)
        };
        var tokenHandler = new JwtSecurityTokenHandler();
        var authToken = tokenHandler.CreateToken(tokenDescriptor);
        
        tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti,
                    jti.ToString())
            }),
            Expires = DateTime.UtcNow.AddSeconds(jwtOptions.ExpirationTime),
            Issuer = issuer,
            Audience = audience,
            SigningCredentials = new SigningCredentials
            (new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha512Signature)
        };
        var refreshToken = tokenHandler.CreateToken(tokenDescriptor);
        
        return new Authorization()
        {
            AuthorizationToken = tokenHandler.WriteToken(authToken),
            RefreshToken = tokenHandler.WriteToken(refreshToken),
            ExpirationTime = jwtOptions.ExpirationTime
        };
    }
}