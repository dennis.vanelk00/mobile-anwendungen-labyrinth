﻿using Microsoft.EntityFrameworkCore;
using MoaServer.Model.V1.Authorization;
using MoaServer.Model.V1.Common;

namespace MoaServer.Model.V1.EFCore;

public class MoaContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<InvalidatedJwt> InvalidatedJwts { get; set; }
    public DbSet<HighScore> HighScores { get; set; }

    public MoaContext()
    {
        
    }
    public MoaContext(DbContextOptions options) : base(options)
    {
        
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder builder)
    {
        if(builder.IsConfigured) return;
        {
            builder.UseNpgsql("User ID=postgres;Password=Test123!;Host=localhost;Port=5432;Database=moa-db;Pooling=true;")
                .UseSnakeCaseNamingConvention();
        }
    }
}