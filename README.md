# MOA Source Code
Mobile Anwendungen, source code

## Entwicklungsumgebung aufsetzen
Vorrausetzungen:
- Node JS LTS (^16.17.X)
- .Net SDK 6.X.X
- Draw.io (Diagramme und Dokumentation)
- Docker Desktop

### Android SDK
Installiere Android Studio mit allen SDK's

### Docker Desktop
Installiere Docker Desktop, dafür wird Hyper V und WSL 2 Benötigt. Einfach der Anleitung des Installationsprogramm folgen.

### Globale Node-Modules
- ignite-cli

`npm install [module-name] -g`

## Tutorials
- [Expo](https://docs.expo.dev/tutorial/planning/)
- [React Native](https://reactnative.dev/docs/tutorial)
- [Ignite (Readme)](https://www.npmjs.com/package/ignite-cli)

## Vorgeschlagene Editoren
- Rider (Backend Code)
- WebStorm (Client Code)
- DataGrip (Datenbank interaktionen)
- Visual Studio Code (Genereller Editor)
- Android Studio (App Testing und SDK installation)

## Umgebung starten
- Den Ordner mit der "docker-compose.yml" Datei in der Konsole öffnen und den Befehl `docker compose up --build` ausführen
- Den MoaClient Ordner mit Webstorm öffnen und den Play Button drücken (Konfiguration 'Launch Expo Web' auswählen)